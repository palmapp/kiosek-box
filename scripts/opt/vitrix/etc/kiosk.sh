#!/bin/bash

xset -dpms
xset s off
openbox-session &
start-pulseaudio-x11

xrandr -s 1920x1080

at_term() {
	kill -s SIGTERM $!
    exit 0
}
trap at_term TERM


while true; do

	if [ -f "/opt/vitrix/data/before-chrome.sh" ]; then
		/bin/bash /opt/vitrix/data/before-chrome.sh
	fi
  
	chrome_extension_flag=""
	if [ -f "/opt/vitrix/data/chrome-extension.link" ]; then
	  read chrome_extension_path < "/opt/vitrix/data/chrome-extension.link"
	fi

	if [ -z "$chrome_extension_path" ] && [ -f "/opt/vitrix/data/chrome-extension.link.old" ]; then
	  read chrome_extension_path < "/opt/vitrix/data/chrome-extension.link.old"
	fi

	if [ -n "$chrome_extension_path" ]; then
	  chrome_extension_flag=--load-extension=$chrome_extension_path
	fi

  rm -rf ~/.{config,cache}/google-chrome/
  google-chrome --kiosk --enable-zero-copy --disable-gpu-driver-bug-workarounds --enable-native-gpu-memory-buffers --no-first-run --disable-pinch --overscroll-history-navigation=0 $chrome_extension_flag 'http://127.0.0.1:10000/screen/screen.html'
done
